package com.pidhornyi.alex.calculator.Calculator;

public class Strings
{
    public static String SEPARATOR = " ";
    public static String PLUS = "+";
    public static String MINUS = "-";
    public static String MULT = "*";
    public static String DIV = "/";
    public static String EQ = "=";
    public static String OPEN_BRACKET = "(";
    public static String CLOSE_BRACKET = ")";
    public static String POW = "^";
    public static String LN = "ln";
    public static String SIN = "sin";
    public static String COS = "cos";
    public static String ASIN = "asin";
    public static String ACOS = "acos";
    public static String P = "π";
    public static String E = "e";

    public static String fromOperation(Operation o)
    {
        if(o == Operation.plus)
            return PLUS;
        else if(o == Operation.minus)
            return MINUS;
        else if(o == Operation.mult)
            return MULT;
        else if(o == Operation.div)
            return DIV;
        else if(o == Operation.bropen)
            return OPEN_BRACKET;
        else if(o == Operation.brclose)
            return CLOSE_BRACKET;
        else if(o == Operation.pow)
            return POW;
        else if(o == Operation.ln)
            return LN;
        else if(o == Operation.sin)
            return SIN;
        else if(o == Operation.cos)
            return COS;
        else if(o == Operation.asin)
            return ASIN;
        else if(o == Operation.acos)
            return ACOS;
        else if(o == Operation.p)
            return P;
        else if(o == Operation.e)
            return E;
        return "";
    }

    public static Operation parseOperation(char c) {
        return parseOperation(Character.toString(c));
    }

    public static Operation parseOperation(String string) {
        if(string.equals(PLUS)) {
            return Operation.plus;
        }
        else if(string.equals(MINUS)) {
            return Operation.minus;
        }
        else if(string.equals(MULT)) {
            return Operation.mult;
        }
        else if(string.equals(DIV)) {
            return Operation.div;
        }
        else if(string.equals(EQ)) {
            return Operation.equals;
        }
        else if(string.equals(OPEN_BRACKET)) {
            //return Operation.minus;
        }
        else if(string.equals(CLOSE_BRACKET)) {
            //return Operation.minus;
        }
        else if(string.equals(POW)) {
            return Operation.pow;
        }
        else if(string.equals(LN)) {
            return Operation.ln;
        }
        else if(string.equals(SIN)) {
            return Operation.sin;
        }
        else if(string.equals(COS)) {
            return Operation.cos;
        }
        else if(string.equals(ASIN)) {
            return Operation.asin;
        }
        else if(string.equals(ACOS)) {
            return Operation.acos;
        }
        else if(string.equals(P)) {
            return Operation.p;
        }
        else if(string.equals(E)) {
            return Operation.e;
        }
        return Operation.none;
    }
}
