package com.pidhornyi.alex.calculator.Calculator;


public class Priorities
{
    public static int NUMBER = 0;
    public static int EQUALS = 0;
    public static int PLUS = 4;
    public static int MINUS = 4;
    public static int MULT = 3;
    public static int DIV = 3;
    public static int POW = 2;
    public static int LN = 1;
    public static int P = 0;
    public static int E = 0;
    public static int SIN = 1;
    public static int COS = 1;
    public static int ASIN = 1;
    public static int ACOS = 1;
    public static int GetPriority(Operation o)
    {
        int res = -1;
        switch (o)
        {
            case div:
                res = DIV;
                break;
            case mult:
                res = MULT;
                break;
            case plus:
                res = PLUS;
                break;
            case minus:
                res = MINUS;
                break;
            case equals:
                res = EQUALS;
                break;
            case pow:
                res = POW;
                break;
            case ln:
                res = LN;
                break;
            case sin:
                res = SIN;
                break;
            case cos:
                res = COS;
                break;
            case asin:
                res = ASIN;
                break;
            case acos:
                res = ACOS;
                break;
            case p:
                res = P;
                break;
            case e:
                res = E;
                break;
        }
        return res;
    }
}