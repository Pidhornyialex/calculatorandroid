package com.pidhornyi.alex.calculator.Calculator.Interpreter;

import com.pidhornyi.alex.calculator.Calculator.Node;
import com.pidhornyi.alex.calculator.Calculator.NumberNode;
import com.pidhornyi.alex.calculator.Calculator.Operation;

/**
 * Created by Alex on 1/28/2017.
 */

public class InputNumberNode extends InputNode {
    private String sign = "";
    @Override
    public void addNum(char c) {
        if(c == '.')
        {
            if(!value.contains(Character.toString(c)))
            {
                value += c;
            }
        }
        else
            value += c;
    }

    @Override
    public void addOperation(Operation operation) {
        InputOperatorNode ion = new InputOperatorNode();
        ion.addOperation(operation);
        addNext(ion);
    }

    @Override
    public void switchSign() {
        if (sign.length() == 0)
            sign = "-";
        else
            sign = "";
    }


    @Override
    public Node produceCalcNode() {
        return new NumberNode(Double.parseDouble(sign + value));
    }

    @Override
    public void backspace() {
        value = value.substring(0, value.length() - 1);
        if(value.length() == 0)
        {
            if(previous != null)
                previous.next = null;
            previous = null;
        }
    }

    @Override
    public String toString() {
        return sign + value;
    }
}
