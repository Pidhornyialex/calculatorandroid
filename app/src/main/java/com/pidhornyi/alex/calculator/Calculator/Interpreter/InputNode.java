package com.pidhornyi.alex.calculator.Calculator.Interpreter;

import com.pidhornyi.alex.calculator.Calculator.Node;
import com.pidhornyi.alex.calculator.Calculator.Operation;

/**
 * Created by Alex on 1/28/2017.
 */

public abstract class InputNode {
    protected InputNode previous;
    protected InputNode next;
    protected String value = "";
    public abstract void addNum(char c);
    public abstract void addOperation(Operation operation);
    public void addNext(InputNode n)
    {
        this.next = n;
        n.previous = this;
    }
    public void addPrev(InputNode n)
    {
        this.previous = n;
        n.next = this;
    }
    public InputNode last()
    {
        if(next == null)
            return this;
        else
            return  next.last();
    }

    public abstract void switchSign();
    public abstract Node produceCalcNode();
    public abstract void backspace();

    public abstract String toString();
}
