package com.pidhornyi.alex.calculator.Calculator;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alex on 12/25/2016.
 */

public abstract class Node
{
    protected List<Node> children;

    public Node()
    {
        children = new ArrayList();
    }

    public abstract void AddChild(Node child);

    public abstract double Resolve(boolean safe) throws ArgumentMissingException;
    public abstract int GetPriority();
}