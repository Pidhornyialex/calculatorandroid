package com.pidhornyi.alex.calculator;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.TextView;
import android.widget.Toast;

import com.pidhornyi.alex.calculator.Calculator.ArgumentMissingException;
import com.pidhornyi.alex.calculator.Calculator.Interpreter.Interpreter;
import com.pidhornyi.alex.calculator.Calculator.Operation;

public class Main extends FragmentActivity {
    private Interpreter in;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Remove title bar
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        in = new Interpreter();

        //Remove notification bar

        //set content view AFTER ABOVE sequence (to avoid crash)
        setContentView(R.layout.activity_main);
    }

    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.btnZero:
                in.addNumber('0');
                break;
            case R.id.btnOne:
                in.addNumber('1');
                break;
            case R.id.btnTwo:
                in.addNumber('2');
                break;
            case R.id.btnThree:
                in.addNumber('3');
                break;
            case R.id.btnFour:
                in.addNumber('4');
                break;
            case R.id.btnFive:
                in.addNumber('5');
                break;
            case R.id.btnSix:
                in.addNumber('6');
                break;
            case R.id.btnSeven:
                in.addNumber('7');
                break;
            case R.id.btnEight:
                in.addNumber('8');
                break;
            case R.id.btnNine:
                in.addNumber('9');
                break;
            case R.id.btnDot:
                in.addNumber('.');
                break;
            case R.id.btnPlus:
                in.addOperation(Operation.plus);
                break;
            case R.id.btnMinus:
                in.addOperation(Operation.minus);
                break;
            case R.id.btnMult:
                in.addOperation(Operation.mult);
                break;
            case R.id.btnDiv:
                in.addOperation(Operation.div);
                break;
            case R.id.btnPow:
                in.addOperation(Operation.pow);
                break;
            case R.id.btnOpen:
                in.addOperation(Operation.bropen);
                break;
            case R.id.btnClose:
                in.addOperation(Operation.brclose);
                break;
            case R.id.btnSign:
                in.switchSign();
                break;
            case R.id.btnErase:
                in.backspace();
                break;
            case R.id.btnEq:
                try {
                    String temp = fmt(in.toCalculatorLogic().Resolve(true));
                    in.clear();
                    for(int i = 0; i < temp.length(); i++)
                    {
                        in.addNumber(temp.charAt(i));
                    }

                } catch (ArgumentMissingException e) {
                    e.printStackTrace();
                }
                break;
            case R.id.btnAC:
                in.clear();
                break;
            case R.id.btnSciInput:
                Toast toast = Toast.makeText(this, "Scientific inputs coming soon!", Toast.LENGTH_SHORT);
                toast.show();
                break;

        }
        TextView tv = (TextView)findViewById(R.id.textView);
        try {
            tv.setText(fmt(in.toCalculatorLogic().Resolve(true)));
        } catch (ArgumentMissingException e) {
            e.printStackTrace();
        }
        TextView tv2 = (TextView)findViewById(R.id.textView2);
        tv2.setText(in.toText());
    }
    public void onTouchDown(View v) {
        int abc = 0;
    }
    public void onTouchUp(View v) {
        int abc = 0;
    }

    private static String fmt(double d)
    {
        if(d == (long) d)
            return String.format("%d",(long)d);
        else
            return String.format("%s",d);
    }

}
