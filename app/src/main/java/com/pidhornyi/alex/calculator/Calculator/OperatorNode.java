package com.pidhornyi.alex.calculator.Calculator;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alex on 12/25/2016.
 */


public class OperatorNode extends Node {
    private Operation operation;

    public OperatorNode(Operation op) {
        operation = op;
    }

    public int GetPriority() {
        return Priorities.GetPriority(operation);
    }

    public void AddChild(Node child) {
        if (children.size() < Numbers.GetNumber(operation)) {
            children.add(child);
        } else {
            if (children.get(children.size() - 1).GetPriority() > child.GetPriority()) {
                children.get(children.size() - 1).AddChild(child);
            } else {
                Node last = children.get(children.size() - 1);
                children.remove(children.size() - 1);
                children.add(child);
                child.AddChild(last);
            }
        }
    }

    public double Resolve(boolean safe) throws ArgumentMissingException {
        double res = 0;
        List<Double> numbers = ResolveChildren(Numbers.GetNumber(operation), safe);
        boolean error = numbers.size() < Numbers.GetNumber(operation);
        switch (operation) {
            case equals:

                if (error) {
                    if (safe)
                        res = 0;
                    else
                        throw new ArgumentMissingException();
                } else {
                    res = numbers.get(0);
                }
                break;
            case plus:
                if (error) {
                    if (safe) {
                        if (numbers.size() > 0)
                            res = numbers.get(0);
                        else
                            throw new ArgumentMissingException();
                    } else
                        throw new ArgumentMissingException();
                } else {
                    res = numbers.get(0) + numbers.get(1);
                }
                break;
            case minus:
                if (error) {
                    if (safe) {
                        if (numbers.size() > 0)
                            res = numbers.get(0);
                        else
                            throw new ArgumentMissingException();
                    } else
                        throw new ArgumentMissingException();
                } else {
                    res = numbers.get(0) - numbers.get(1);
                }
                break;
            case mult:
                if (error) {
                    if (safe) {
                        if (numbers.size() > 0)
                            res = numbers.get(0);
                        else
                            throw new ArgumentMissingException();
                    } else
                        throw new ArgumentMissingException();
                } else {
                    res = numbers.get(0) * numbers.get(1);
                }
                break;
            case div:
                if (error) {
                    if (safe) {
                        if (numbers.size() > 0)
                            res = numbers.get(0);
                        else
                            throw new ArgumentMissingException();
                    } else
                        throw new ArgumentMissingException();
                } else {
                    res = numbers.get(0) / numbers.get(1);
                }
                break;
            case pow:
                if (error)
                {
                    if (safe)
                    {
                        if (numbers.size() > 0)
                            res = numbers.get(0);
                        else
                            throw new ArgumentMissingException();
                    }
                    else
                        throw new ArgumentMissingException();
                }
                else
                {
                    res = Math.pow(numbers.get(0), numbers.get(1));
                }
                break;
            case ln:
                if (error)
                {
                    throw new ArgumentMissingException();
                }
                else
                {
                    res = Math.log(numbers.get(0));
                }
                break;
            case sin:
                if (error)
                {
                    throw new ArgumentMissingException();
                }
                else
                {
                    res = Math.sin(numbers.get(0));
                }
                break;
            case cos:
                if (error)
                {
                    throw new ArgumentMissingException();
                }
                else
                {
                    res = Math.cos(numbers.get(0));
                }
                break;
            case asin:
                if (error)
                {
                    throw new ArgumentMissingException();
                }
                else
                {
                    res = Math.asin(numbers.get(0));
                }
                break;
            case acos:
                if (error)
                {
                    throw new ArgumentMissingException();
                }
                else
                {
                    res = Math.acos(numbers.get(0));
                }
                break;
            case p:
                res = Math.PI;
                break;
            case e:
                res = Math.E;
                break;
        }

        return res;
    }

    private List<Double> ResolveChildren(int numberOfChildren, boolean safe) {
        List<Double> res = new ArrayList<>();
        for (int i = 0; i < numberOfChildren; i++) {
            if (children.size() > i) {
                try {
                    res.add(children.get(i).Resolve(safe));
                } catch (ArgumentMissingException e) {

                }
            }
        }
        return res;
    }
}