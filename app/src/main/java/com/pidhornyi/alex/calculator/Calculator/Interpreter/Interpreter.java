package com.pidhornyi.alex.calculator.Calculator.Interpreter;

import com.pidhornyi.alex.calculator.Calculator.Node;
import com.pidhornyi.alex.calculator.Calculator.Operation;
import com.pidhornyi.alex.calculator.Calculator.OperatorNode;

import java.util.ArrayList;

/**
 * Created by Alex on 1/4/2017.
 */

public class Interpreter
{
    private InputNode head;
    private ArrayList<Node> keepingTrackOfBrackets = new ArrayList();

    public Interpreter(){
        head = new InputOperatorNode();
        head.addOperation(Operation.equals);
    }

    public void addNumber(char c)
    {
        head.last().addNum(c);
    }

    public void addOperation(Operation o)
    {
        head.last().addOperation(o);
    }

    public void backspace()
    {
        head.last().backspace();
    }

    public void clear()
    {
        head = new InputOperatorNode();
        head.addOperation(Operation.equals);
    }

    public void switchSign()
    {
        head.last().switchSign();
    }

    public Node toCalculatorLogic()
    {
        InputNode current = head;
        Node res = new OperatorNode(Operation.equals);
        keepingTrackOfBrackets.add(res);
        while(current != null)
        {
            if(current instanceof InputOperatorNode) {
                if (((InputOperatorNode) current).getOperation() == Operation.bropen) {
                    OperatorNode opbr = new OperatorNode(Operation.equals);
                    keepingTrackOfBrackets.get(keepingTrackOfBrackets.size() - 1).AddChild(opbr);
                    keepingTrackOfBrackets.add(opbr);
                } else if (((InputOperatorNode) current).getOperation() == Operation.brclose) {
                    if(keepingTrackOfBrackets.size() > 1)
                        keepingTrackOfBrackets.remove(keepingTrackOfBrackets.size() - 1);
                }
                else
                {
                    keepingTrackOfBrackets.get(keepingTrackOfBrackets.size() - 1).AddChild(current.produceCalcNode());
                }
            }
            else
            {
                keepingTrackOfBrackets.get(keepingTrackOfBrackets.size() - 1).AddChild(current.produceCalcNode());
            }
            current = current.next;
        }
        return res;
    }

    public String toText()
    {
        String res = "";
        InputNode current = head;
        while(current != null)
        {
            res += current.toString();
            current = current.next;
        }
        return res;
    }

}

