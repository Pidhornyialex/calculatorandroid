package com.pidhornyi.alex.calculator.Calculator.Interpreter;

import com.pidhornyi.alex.calculator.Calculator.Node;
import com.pidhornyi.alex.calculator.Calculator.Numbers;
import com.pidhornyi.alex.calculator.Calculator.Operation;
import com.pidhornyi.alex.calculator.Calculator.OperatorNode;
import com.pidhornyi.alex.calculator.Calculator.Strings;

/**
 * Created by Alex on 1/28/2017.
 */

public class InputOperatorNode extends InputNode {
    private Operation operation = Operation.none;

    @Override
    public void addNum(char c) {
        InputNumberNode inn = new InputNumberNode();
        inn.addNum(c);
        addNext(inn);
    }

    @Override
    public void addOperation(Operation o) {
        if(operation == Operation.none)
        {
            operation = o;
        }
        else if(o == Operation.bropen || o == Operation.brclose || operation == Operation.brclose)
        {
            InputOperatorNode ion = new InputOperatorNode();
            ion.addOperation(o);
            addNext(ion);
        }
        else if(Numbers.GetNumber(o) == 0)
        {
            if(o == Operation.none)
                operation = o;
            else
            {
                InputOperatorNode ion = new InputOperatorNode();
                ion.addOperation(o);
                addNext(ion);
            }
        }
        else
            operation = o;
    }

    @Override
    public void switchSign() {

    }

    @Override
    public Node produceCalcNode() {
        OperatorNode on = new OperatorNode(operation);
        return on;
    }

    @Override
    public void backspace() {
        if(operation != Operation.equals) {
            if (previous != null)
                previous.next = null;
            previous = null;
        }
    }

    @Override
    public String toString() {
        return Strings.fromOperation(operation);
    }

    public Operation getOperation(){
        return operation;
    }
}
