package com.pidhornyi.alex.calculator.Calculator;


/**
 * Created by Alex on 12/25/2016.
 */

public class NumberNode extends Node {
    private double value;

    public NumberNode(double v) {
        value = v;
    }

    public void AddChild(Node child) {
        children.add(child);
    }

    public int GetPriority() {
        return Priorities.NUMBER;
    }

    public double Resolve(boolean safe) {
        return value;
    }
}
