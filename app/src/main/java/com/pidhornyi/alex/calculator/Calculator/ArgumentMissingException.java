package com.pidhornyi.alex.calculator.Calculator;

public class ArgumentMissingException extends Exception
{
    //Parameterless Constructor
    public ArgumentMissingException() {}

    //Constructor that accepts a message
    public ArgumentMissingException(String message)
    {
        super(message);
    }
}
